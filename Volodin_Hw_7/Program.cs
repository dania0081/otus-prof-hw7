﻿using System.Diagnostics;

namespace Volodin_Hw_7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            string[] files = { "file1.txt", "file2.txt", "file3.txt" };
            foreach (string file in files)
            {
                CreateFile(file, "This is a file.");
            }
            
            string folderPath = Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\..", "Texts");
            
            int totalSpaces = CountSpacesParallel(folderPath);

            Console.WriteLine($"Total spaces: {totalSpaces}");

            stopwatch.Stop();

            Console.WriteLine($"Execution time: {stopwatch.ElapsedMilliseconds} ms");
        }

        public static int CountSpacesParallel(string path)
        {
            int totalSpaces = 0;
            string[] files = Directory.GetFiles(path);
            Task[] tasks = new Task[files.Length];

            for (int i = 0; i < files.Length; i++)
            {
                string file = files[i];

                tasks[i] = Task.Run(() =>
                {
                    int spaces = CountSpacesInFile(file);
                    totalSpaces += spaces;
                });
            }

            Task.WaitAll(tasks);

            return totalSpaces;
        }

        public static int CountSpacesInFile(string path)
        {
            int spaces = 0;

            using (StreamReader reader = new StreamReader(path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    spaces += line.Split(' ').Length - 1;
                }
            }

            return spaces;
        }

        public static void CreateFile(string fileName, string text)
        {
            File.WriteAllText(fileName, text);
        }
    }
}